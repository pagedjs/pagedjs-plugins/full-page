# Full page elements (paged.js)

This script help you to create full page elements and full spread elements in the flow of your book. It's an handlers for paged.js.

You need to use [csstree.js](https://github.com/csstree/csstree) in order to transform custom properties. 


## How to use it

Add csstree in your HTML `<head>`: 

```javascript
<script src="js/csstree.min.js"></script>
```

Add the script in your HTML `<head>` (after the paged.js script): 

```javascript
<script src="js/full-page.js"></script>
```


In the CSS, on the element(s) you want in full page add the following custom property(works with id and classes):

```css
elem{
    --pagedjs-full-page: page
}
```

You have multiple keywords for the custom property:
- `--pagedjs-full-page: page` → The element will be remove from flow and put in the next page.
- `--pagedjs-full-page: left` → The element will be remove from flow and put in the next left page.
- `--pagedjs-full-page: right` → The element will be remove from flow and put in the next right page.
- `--pagedjs-full-page: spread` → The element will be remove from flow and put in the next spread.

Note that this script works on any elements, even if the element contains several child elements. 


### Images in full page

If you want an image in full page, we advise you to use the usual `objet-fit` properties.

```css
img {
    object-fit: cover;
    object-position: 0px 0px;
    width: 100%;
    height: 100%;
}
```

- To change the size of you image, use `width` and `height`.
- To change the position of your image, use `object-position`.
- In the case of the `spread` option, all the spread will be considered, i.e. `width: 100%` cover all the spread. 
  

### Spread and central fold

Sometimes, when a book is binding, the elements that cover the spread need to be offset from the central fold. A custom value can be added to the page to take it into account.

```css
@page {
    --pagedjs-fold: 10mm;
}
```

### Bleeds of full page and full spread elements

In order to avoid that your elements moves when you change the bleeds and the crop marks of your document, the bleeds of full page elements was set up to `6mm`. This is due to the way Paged.js modifies the DOM (full page elements are contained in the page sheet and depend on the dimensions of this page sheet). 
If you want to change the dimensions of these specific bleeds, you just have to change the value of the `bleedFull` variable in the first line of the `full-page.js` file


### Examples

You can find examples of use in `css/full-page.css`.

![](images/full-page-example.png)


## Credits

- [pagedjs.org](https://www.pagedjs.org/) (v0.2.0)
- [csstree.js](https://github.com/csstree/csstree)

MIT licence, Julie Blanc, 2021


